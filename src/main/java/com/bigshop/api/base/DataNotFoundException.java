package com.bigshop.api.base;

public class DataNotFoundException extends RuntimeException {
    public DataNotFoundException(Long id) {
        super("Could not find data " + id);
    }
}
