package com.bigshop.api.repositories;

import com.bigshop.api.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long> {
    public List<Product> getProductByDescriptionAndPrice(String description, int price);
}