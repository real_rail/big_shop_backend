package com.bigshop.api.models;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "product")
public class Product {

    public Product(Long id, String title, int price, String image_url, String description, int id_type) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.image_url = image_url;
        this.description = description;
        this.id_type = id_type;
    }

    public Product(){}

    private @Id @GeneratedValue(strategy= GenerationType.AUTO) Long id;
    private String title;
    private int price;
    private String image_url;
    private String description;
    private int id_type;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getId_type() { return id_type; }

    public void setId_type(int id_type) { this.id_type = id_type; }
}