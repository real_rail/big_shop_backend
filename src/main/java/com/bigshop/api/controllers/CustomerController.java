package com.bigshop.api.controllers;

import com.bigshop.api.base.BaseController;
import com.bigshop.api.models.Customer;
import com.bigshop.api.services.CustomerService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RequestMapping("/customers")
@RestController
public class CustomerController extends BaseController {
    private final CustomerService customerService;

    public CustomerController(CustomerService customerService) {
        this.customerService = customerService;
    }

    @GetMapping()
    List<Customer> all() {
        return customerService.getCustomerRepository().findAll();
    }

    @PostMapping()
    Customer newCustomer(@RequestBody Customer newCustomer) {
        return customerService.getCustomerRepository().save(newCustomer);
    }

}
