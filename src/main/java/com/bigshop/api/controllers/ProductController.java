package com.bigshop.api.controllers;

import java.util.List;

import com.bigshop.api.base.BaseController;
import com.bigshop.api.base.DataNotFoundException;
import com.bigshop.api.models.Product;
import com.bigshop.api.repositories.ProductRepository;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/products")
@RestController
class ProductController extends BaseController {

    private final ProductRepository repository;

    ProductController(ProductRepository repository) {
        this.repository = repository;
    }

    // Aggregate root

    @GetMapping()
    List<Product> all() {
        return repository.findAll();
    }

    @PostMapping()
    Product newProduct(@RequestBody Product newProduct) {
        return repository.save(newProduct);
    }

    // Single item

    @GetMapping("/{id}")
    Product one(@PathVariable Long id) {

        return repository.findById(id)
                .orElseThrow(() -> new DataNotFoundException(id));
    }

    @PutMapping("/{id}")
    Product replaceProduct(@RequestBody Product newProduct, @PathVariable Long id) {

        return repository.findById(id)
                .map(Product -> {
                    Product.setTitle(newProduct.getTitle());
                    Product.setDescription(newProduct.getDescription());
                    return repository.save(Product);
                })
                .orElseGet(() -> {
                    newProduct.setId(id);
                    return repository.save(newProduct);
                });
    }

    @DeleteMapping("/{id}")
    void deleteProduct(@PathVariable Long id) {
        repository.deleteById(id);
    }
}